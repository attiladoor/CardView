package info.androidhive.cardview;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;





public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {

    private Context mContext;
    private List<ListItem> itemList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView listElement;

        public MyViewHolder(View view) {
            super(view);
            listElement = (TextView) view.findViewById(R.id.list_element);
        }
    }


    public ListAdapter(Context mContext, List<ListItem> itemList) {
        this.mContext = mContext;
        this.itemList = itemList;
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        ListItem listItem = itemList.get(position);
        holder.listElement.setText(listItem.getListElement());
        //holder.count.setText(album.getNumOfSongs() + " songs");

        // loading album cover using Glide library

    }


    /**
     * Showing popup menu when tapping on 3 dots
     */





    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {



        }



        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            /*switch (menuItem.getItemId()) {
                case R.id.action_add_favourite:
                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_play_next:
                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }*/
            Toast.makeText(mContext, "Content is saved", Toast.LENGTH_SHORT).show();



            return false;
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}

