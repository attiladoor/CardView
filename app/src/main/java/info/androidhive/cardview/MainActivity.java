package info.androidhive.cardview;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.xml.sax.*;
import org.w3c.dom.*;




import com.bumptech.glide.Glide;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;

public class MainActivity extends AppCompatActivity {

    public static RecyclerView recyclerView;
    private static AlbumsAdapter adapter1;
    private static ListAdapter adapter2;
    private List<Album> albumList;
    public static Context mContext;
    private static RecyclerView.LayoutManager mLayoutManager2;
    RecyclerView.LayoutManager mLayoutManager;
    private static ArrayList<String> namev,valuev;







    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        namev = null; valuev = null;
        ///////////////permissions

        getPermission(android.Manifest.permission.READ_CONTACTS);
        getPermission(Manifest.permission.ACCESS_WIFI_STATE);
        getPermission(Manifest.permission.ACCESS_NETWORK_STATE);
        getPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        getPermission(Manifest.permission.READ_PHONE_STATE);
        getPermission(Manifest.permission.BATTERY_STATS);
        getPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        ////////////////////////
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();

        mContext = getApplicationContext();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();
        adapter1 = new AlbumsAdapter(this, albumList);

        mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter1);

        prepareAlbums();

        try {
            Glide.with(this).load(R.drawable.cover3).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.album1,
                R.drawable.album2,
                R.drawable.album3,
                R.drawable.album4,
                R.drawable.battery,
                R.drawable.album6,
};

        Album a = new Album("Device", 13, covers[0]);
        albumList.add(a);

        a = new Album("Wifi", 8, covers[1]);
        albumList.add(a);

        a = new Album("Cellular", 11, covers[2]);
        albumList.add(a);

        a = new Album("SIM", 12, covers[3]);
        albumList.add(a);

        a = new Album("Battery", 14, covers[4]);
        albumList.add(a);

        a = new Album("Save to XML", 1, covers[5]);
        albumList.add(a);


        adapter1.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }



    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static void setAdapter2(String title){

        List<ListItem> itemList;
        itemList = new ArrayList<>();





        ListItem a;

        adapter2 = new ListAdapter(mContext,itemList);

        //LinearLayoutManager manager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        //manager.canScrollHorizontally();
        mLayoutManager2 = new GridLayoutManager(mContext, 6,HORIZONTAL,false);
        recyclerView.setLayoutManager(mLayoutManager2);
        recyclerView.setAdapter(adapter2);





        if (title.equals("Device")) {

                a = new ListItem("Device Info:");
                itemList.add(a);

                a = new ListItem("Brand:" + android.os.Build.MANUFACTURER);
                itemList.add(a);


                a = new ListItem("Model: " + android.os.Build.MODEL);
                itemList.add(a);


                a = new ListItem("IMEI: " + DeviceInfo.getDeviceInfo(mContext, "IMEI"));
                itemList.add(a);


                a = new ListItem("Display: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_IN_INCH") + "\"");
                itemList.add(a);


                a = new ListItem("CPU cores: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NUMBER_OF_PROCESSORS"));
                itemList.add(a);


                a = new ListItem("Total RAM: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_TOTAL_MEMORY") + " MB");
                itemList.add(a);


                a = new ListItem("Free RAM: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_FREE_MEMORY") + " MB");
                itemList.add(a);


                a = new ListItem("Used RAM: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_USED_MEMORY") + " MB");
                itemList.add(a);


                a = new ListItem("Total CPU usage: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_TOTAL_CPU_USAGE") + "%");
                itemList.add(a);



            }
            if (title.equals("Wifi")) {

                a = new ListItem("Wifi:");
                itemList.add(a);

                a = new ListItem("Wifi state: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_ENABLED"));
                itemList.add(a);


                a = new ListItem("SSID: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_SSID"));
                itemList.add(a);


                a = new ListItem("MAC address (wlan): " + DeviceInfo.getDeviceInfo(mContext, "WIFI_MAC"));
                itemList.add(a);


                a = new ListItem("IP address: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_IP"));
                itemList.add(a);


                a = new ListItem("Link speed: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_SPEED")+" Mbps");
                itemList.add(a);


                a = new ListItem("Signal strength: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_STRENGTH")+" dbm");
                itemList.add(a);

                a = new ListItem("DNS1: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_DNS1"));
                itemList.add(a);


                a = new ListItem("DNS2: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_DNS2"));
                itemList.add(a);


                a = new ListItem("Gateway: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_GATEWAY"));
                itemList.add(a);


                a = new ListItem("DHCP: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_DHCP"));
                itemList.add(a);


                a = new ListItem("Netmask: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_NETMASK"));
                itemList.add(a);


                a = new ListItem("leaseDuration: " + DeviceInfo.getDeviceInfo(mContext, "WIFI_LEASEDURATION")+" sec");
                itemList.add(a);


            }

            if (title.equals("Cellular")) {

                a = new ListItem("Cellular:");
                itemList.add(a);

                a = new ListItem("Network type: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_TYPE"));
                itemList.add(a);

                a = new ListItem("Network subtype: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_EXTRAINFO"));
                itemList.add(a);

                a = new ListItem("Network operator: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_OPERATORNAME"));
                itemList.add(a);


                a = new ListItem("Network class: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_CLASS"));
                itemList.add(a);

                a = new ListItem("CID: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_CID"));
                itemList.add(a);

                a = new ListItem("MCC: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_MMC"));
                itemList.add(a);

                a = new ListItem("LAC: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_LAC"));
                itemList.add(a);

                a = new ListItem("MMS user agent: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_MMSAGENT"));
                itemList.add(a);

                a = new ListItem("Network is roaming: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_ISROAMING"));
                itemList.add(a);


                a = new ListItem("Network metered: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_METERED"));
                itemList.add(a);


            }

            if (title.equals("SIM")) {

                a = new ListItem("Sim");
                itemList.add(a);

                a = new ListItem("Operator: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_OPERATOR"));
                itemList.add(a);

                a = new ListItem("Country: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_COUNTRYISO"));
                itemList.add(a);

                a = new ListItem("Serial number: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_SERIELNUM"));
                itemList.add(a);

                a = new ListItem("State: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_STATE"));
                itemList.add(a);

                a = new ListItem("IMSI: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_IMSI"));
                itemList.add(a);
            }

            if (title.equals("Battery")) {

                a = new ListItem("Battery");
                itemList.add(a);

                a = new ListItem("Is charing: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_ISCHARGING"));
                itemList.add(a);

                a = new ListItem("Capacity[%]: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CAPACITY"));
                itemList.add(a);

                a = new ListItem("Capacity[mAh]: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CHARGE_COUNTER"));
                itemList.add(a);

                a = new ListItem("Curreny(now): " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CURRENT_NOW") +" mA");
                itemList.add(a);

                a = new ListItem("Health: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTTERY_HEALTH"));
                itemList.add(a);

                a = new ListItem("Technology: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_TECHNOLOGY"));
                itemList.add(a);

                a = new ListItem("Temperature: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_TEMPERATURE")+" C");
                itemList.add(a);

                a = new ListItem("Voltage: " + DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_VOLTAGE")+" V");
                itemList.add(a);

            }


            if (title.equals("Saved files")) {

                a = new ListItem("files");
                itemList.add(a);
                saveToXML("asd");
            }


            adapter2.notifyDataSetChanged();


       // Toast.makeText(mContext, "Button Clicked", Toast.LENGTH_SHORT).show();
    }



    @Override
    public boolean onKeyDown(int keyKode, KeyEvent event){

        if(keyKode == KeyEvent.KEYCODE_BACK && recyclerView.getAdapter().equals(adapter2)){

            mLayoutManager = new GridLayoutManager(mContext, 2);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(adapter1);
            return true;


        }

        return super.onKeyDown(keyKode, event);
        //return true;
    }







    public void getPermission(String PERMISSION) {
        int CALL_BACK =1;

        if (ContextCompat.checkSelfPermission(this, PERMISSION)!= PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{ PERMISSION},CALL_BACK);
        }
    }

    public static void saveToXML(String xml) {
        Document dom;
        Element e = null;




        // instance of a DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            // use factory to get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            // create instance of DOM
            dom = db.newDocument();

            // create the root element
            Element rootEle = dom.createElement("INFO");
            dom.appendChild(rootEle);

            Element subEle = dom.createElement("DEVICE");
            rootEle.appendChild(subEle);

                       // create data elements and place them under root
            e = dom.createElement("BRAND");
            e.appendChild(dom.createTextNode(android.os.Build.MANUFACTURER));
            subEle.appendChild(e);

            e = dom.createElement("MODEL");
            e.appendChild(dom.createTextNode(android.os.Build.MODEL));
            subEle.appendChild(e);

            e = dom.createElement("IMEI");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "IMEI")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_IN_INCH");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_IN_INCH")));
            subEle.appendChild(e);

            e = dom.createElement("IMDEVICE_NUMBER_OF_PROCESSORSEI");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NUMBER_OF_PROCESSORS")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_TOTAL_MEMORY");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_TOTAL_MEMORY")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_FREE_MEMORY");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_FREE_MEMORY")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_USED_MEMORY");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_USED_MEMORY")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_TOTAL_CPU_USAGE");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_TOTAL_CPU_USAGE")));
            subEle.appendChild(e);

            subEle = dom.createElement("WIFI");
            rootEle.appendChild(subEle);

            e = dom.createElement("WIFI_ENABLED");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_ENABLED")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_SSID");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_SSID")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_MAC");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_MAC")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_IP");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_IP")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_SPEED");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_SPEED")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_STRENGTH");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_STRENGTH")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_DNS1");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_DNS1")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_DNS2");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_DNS2")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_GATEWAY");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_GATEWAY")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_DHCP");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_DHCP")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_NETMASK");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_NETMASK")));
            subEle.appendChild(e);

            e = dom.createElement("WIFI_LEASEDURATION");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "WIFI_LEASEDURATION")));
            subEle.appendChild(e);


            subEle = dom.createElement("Cellular");
            rootEle.appendChild(subEle);

            e = dom.createElement("DEVICE_NETWORK_TYPE");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_TYPE")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_EXTRAINFO");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_EXTRAINFO")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_OPERATORNAME");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_OPERATORNAME")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_CLASS");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_CLASS")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_CID");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_CID")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_MMC");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_MMC")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_LAC");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_LAC")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_MMSAGENT");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_MMSAGENT")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_ISROAMING");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_ISROAMING")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_NETWORK_METERED");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_METERED")));
            subEle.appendChild(e);


            subEle = dom.createElement("SIM");
            rootEle.appendChild(subEle);

            e = dom.createElement("DEVICE_SIM_OPERATOR");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_OPERATOR")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_SIM_COUNTRYISO");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_COUNTRYISO")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_SIM_SERIELNUM");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_SERIELNUM")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_SIM_STATE");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_STATE")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_SIM_IMSI");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_IMSI")));
            subEle.appendChild(e);



            subEle = dom.createElement("BATTERY");
            rootEle.appendChild(subEle);


            e = dom.createElement("DEVICE_BATTERY_ISCHARGING");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_ISCHARGING")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_BATTERY_PROPERTY_CAPACITY");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CAPACITY")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_BATTERY_PROPERTY_CHARGE_COUNTER");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CHARGE_COUNTER")));
            subEle.appendChild(e);


            e = dom.createElement("DEVICE_BATTERY_PROPERTY_CURRENT_NOW");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CURRENT_NOW")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_BATTTERY_HEALTH");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTTERY_HEALTH")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_BATTERY_TECHNOLOGY");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_TECHNOLOGY")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_BATTERY_TEMPERATURE");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_TEMPERATURE")));
            subEle.appendChild(e);

            e = dom.createElement("DEVICE_BATTERY_VOLTAGE");
            e.appendChild(dom.createTextNode(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_VOLTAGE")));
            subEle.appendChild(e);




            //dom.appendChild(rootEle);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                // send DOM to file
               // tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(xml)));
                Toast.makeText(mContext, "File is saved to Downloads/report.xml", Toast.LENGTH_SHORT).show();
                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS+"/report.xml");
                file.createNewFile();
                tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(file.getPath())));


            } catch (TransformerException te) {
                System.out.println(te.getMessage());
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } catch (ParserConfigurationException pce) {
            System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
        }
    }

    }





