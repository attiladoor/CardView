package info.androidhive.cardview;

import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import static info.androidhive.cardview.R.id.thumbnail;
import static info.androidhive.cardview.R.id.title;


public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Album> albumList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        public ImageButton thumbnail;
        public ImageView  overflow;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageButton) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);

            thumbnail.setOnClickListener(new View.OnClickListener(){
            public void onClick(final View v){
                MainActivity.setAdapter2(title.getText().toString());
            }
            });
        }
    }


    public AlbumsAdapter(Context mContext, List<Album> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Album album = albumList.get(position);
        holder.title.setText(album.getName());
        //holder.count.setText(album.getNumOfSongs() + " songs");

        // loading album cover using Glide library
        Glide.with(mContext).load(album.getThumbnail()).into(holder.thumbnail);

        holder.overflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.overflow);
            }
        });
    }


    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_album, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }




/*
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {



        }






        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {

            List<String> namev = new ArrayList<String>();
            List<String> valuev = new ArrayList<String>();


            if(menuItem.getTitle().toString().equals("Device")) {
                namev.add("BRAND");
                valuev.add(android.os.Build.MANUFACTURER);

                namev.add("MODEL");
                valuev.add(android.os.Build.MODEL);

                namev.add("IMEI");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "IMEI"));

                namev.add("DEVICE_IN_INCH");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_IN_INCH"));

                namev.add("DEVICE_NUMBER_OF_PROCESSORS");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NUMBER_OF_PROCESSORS"));

                namev.add("DEVICE_TOTAL_MEMORY");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_TOTAL_MEMORY"));

                namev.add("DEVICE_FREE_MEMORY");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_FREE_MEMORY"));

                namev.add("DEVICE_USED_MEMORY");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_USED_MEMORY"));

                namev.add("DEVICE_TOTAL_CPU_USAGE");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_TOTAL_CPU_USAGE"));
            }

            if (menuItem.getTitle().toString().equals("Wifi")) {

                namev.add("WIFI_ENABLED");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_ENABLED"));

                namev.add("WIFI_SSID");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_SSID"));

                namev.add("WIFI_MAC");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_MAC"));

                namev.add("WIFI_IP");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_IP"));


                namev.add("WIFI_SPEED");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_SPEED"));

                namev.add("WIFI_STRENGTH");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_STRENGTH"));

                namev.add("WIFI_DNS1");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_DNS1"));

                namev.add("WIFI_DNS2");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_DNS2"));

                namev.add("WIFI_GATEWAY");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_GATEWAY"));


                namev.add("WIFI_DHCP");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_DHCP"));


                namev.add("WIFI_NETMASK");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_NETMASK"));

                namev.add("WIFI_LEASEDURATION");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "WIFI_LEASEDURATION"));

            }

            if (menuItem.getTitle().equals("Cellular")) {

                namev.add("DEVICE_NETWORK_TYPE");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_TYPE"));


                namev.add("DEVICE_NETWORK_EXTRAINFO");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_EXTRAINFO"));

                namev.add("DEVICE_NETWORK_OPERATORNAME");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_OPERATORNAME"));

                namev.add("DEVICE_NETWORK_CLASS");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_CLASS"));


                namev.add("DEVICE_NETWORK_CID");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_CID"));


                namev.add("DEVICE_NETWORK_MMC");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_MMC"));


                namev.add("DEVICE_NETWORK_LAC");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_LAC"));


                namev.add("DEVICE_NETWORK_MMSAGENT");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_MMSAGENT"));


                namev.add("DEVICE_NETWORK_ISROAMING");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_ISROAMING"));



                namev.add("DEVICE_NETWORK_METERED");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_NETWORK_METERED"));


            }

            if (menuItem.getTitle().equals("SIM")) {

                namev.add("DEVICE_SIM_OPERATOR");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_OPERATOR"));


                namev.add("DEVICE_SIM_COUNTRYISO");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_COUNTRYISO"));


                namev.add("DEVICE_SIM_SERIELNUM");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_SERIELNUM"));


                namev.add("DEVICE_SIM_STATE");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_STATE"));


                namev.add("DEVICE_SIM_IMSI");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_SIM_IMSI"));
            }

            if (menuItem.getTitle().equals("Battery"))

            {


                namev.add("DEVICE_BATTERY_ISCHARGING");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_ISCHARGING"));


                namev.add("DEVICE_BATTERY_PROPERTY_CAPACITY");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CAPACITY"));


                namev.add("DEVICE_BATTERY_PROPERTY_CHARGE_COUNTER");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CHARGE_COUNTER"));


                namev.add("DEVICE_BATTERY_PROPERTY_CURRENT_NOW");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_PROPERTY_CURRENT_NOW"));


                namev.add("DEVICE_BATTTERY_HEALTH");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTTERY_HEALTH"));


                namev.add("DEVICE_BATTERY_TECHNOLOGY");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_TECHNOLOGY"));


                namev.add("DEVICE_BATTERY_TEMPERATURE");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_TEMPERATURE"));


                namev.add("DEVICE_BATTERY_VOLTAGE");
                valuev.add(DeviceInfo.getDeviceInfo(mContext, "DEVICE_BATTERY_VOLTAGE"));

            }
            saveToXML(Environment.DIRECTORY_DOWNLOADS+"/asd.xml", (String) menuItem.getTitle(),namev,valuev);



            return false;
        }
}


    public void saveToXML(String xml,String class_name,List<String> namev,List<String> valuev) {
        Document dom;
        Element e = null;

        // instance of a DocumentBuilderFactory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            // use factory to get an instance of document builder
            DocumentBuilder db = dbf.newDocumentBuilder();
            // create instance of DOM
            dom = db.newDocument();

            // create the root element
            Element rootEle = dom.createElement(class_name);

            // create data elements and place them under root
            /*e = dom.createElement("role1");
            e.appendChild(dom.createTextNode("asd"));
            rootEle.appendChild(e);*/

            /*e = dom.createElement("role2");
            e.appendChild(dom.createTextNode(role2));
            rootEle.appendChild(e);

            e = dom.createElement("role3");
            e.appendChild(dom.createTextNode(role3));
            rootEle.appendChild(e);

            e = dom.createElement("role4");
            e.appendChild(dom.createTextNode(role4));
            rootEle.appendChild(e);*/

            for(int i=0;i < namev.size();i++ )
            {
                e = dom.createElement(namev.get(i));
                e.appendChild(dom.createTextNode(valuev.get(i)));
                rootEle.appendChild(e);

            }

            dom.appendChild(rootEle);

            try {
                Transformer tr = TransformerFactory.newInstance().newTransformer();
                tr.setOutputProperty(OutputKeys.INDENT, "yes");
                tr.setOutputProperty(OutputKeys.METHOD, "xml");
                tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
                tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

                // send DOM to file
                // tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(xml)));
                Toast.makeText(mContext, "File is saved to"+xml, Toast.LENGTH_SHORT).show();
                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS+"/asd.txt");
                file.createNewFile();
                tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(file.getPath())));
                //Toast.makeText(mContext, "Content is saved", Toast.LENGTH_SHORT).show();
                /*OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput(file.getPath(), Context.MODE_PRIVATE));
                outputStreamWriter.write("asd");
                outputStreamWriter.close();*/

            } catch (TransformerException te) {
                System.out.println(te.getMessage());
            } catch (IOException ioe) {
                System.out.println(ioe.getMessage());
            }
        } catch (ParserConfigurationException pce) {
            System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
        }
    }



    @Override
    public int getItemCount() {
        return albumList.size();
    }
}

