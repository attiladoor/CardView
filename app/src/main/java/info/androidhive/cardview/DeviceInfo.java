package info.androidhive.cardview;

/**
 * Created by attila on 2016.11.26..
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.DhcpInfo;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.net.ConnectivityManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.os.Build;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothClass.Device;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

import static android.content.Context.WIFI_SERVICE;
//import org.apache.http.conn.util.InetAddressUtils;



public class DeviceInfo {

    public static String getDeviceInfo(Context activity, String device) {
        try {
            switch (device) {
                case "DEVICE_LANGUAGE":
                    return Locale.getDefault().getDisplayLanguage();
                case "DEVICE_TIME_ZONE":
                    return TimeZone.getDefault().getID();//(false, TimeZone.SHORT);
                case "DEVICE_LOCAL_COUNTRY_CODE":
                    return activity.getResources().getConfiguration().locale.getCountry();
                case "DEVICE_CURRENT_YEAR":
                    return "" + (Calendar.getInstance().get(Calendar.YEAR));
                case "DEVICE_CURRENT_DATE_TIME":
                    Calendar calendarTime = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
                    long time = (calendarTime.getTimeInMillis() / 1000);
                    return String.valueOf(time);
                //                    return DateFormat.getDateTimeInstance().format(new Date());
                case "DEVICE_CURRENT_DATE_TIME_ZERO_GMT":
                    Calendar calendarTime_zero = Calendar.getInstance(TimeZone.getTimeZone("GMT+0"), Locale.getDefault());
                    return String.valueOf((calendarTime_zero.getTimeInMillis() / 1000));
                //                    DateFormat df = DateFormat.getDateTimeInstance();
                //                    df.setTimeZone(TimeZone.getTimeZone("GMT+0"));
                //                    return df.format(new Date());
                case "DEVICE_HARDWARE_MODEL":
                    return getDeviceName();
                case "DEVICE_NUMBER_OF_PROCESSORS":
                    return Runtime.getRuntime().availableProcessors() + "";







                case "DEVICE_LOCALE":
                    return Locale.getDefault().getISO3Country();
                case "DEVICE_IP_ADDRESS_IPV4":
                    return getIPAddress(true);
                case "DEVICE_IP_ADDRESS_IPV6":
                    return getIPAddress(false);
                case "DEVICE_MAC_ADDRESS":
                    String mac = getMACAddress("wlan0");
                    if (TextUtils.isEmpty(mac)) {
                        mac = getMACAddress("eth0");
                    }
                    if (TextUtils.isEmpty(mac)) {
                        mac = "DU:MM:YA:DD:RE:SS";
                    }
                    return mac;
                case "WIFI_IP":
                    return getWifiIP(activity);
                case "WIFI_MAC":
                    return getWifiMAC(activity);
                case "WIFI_ENABLED":
                    return getWifiStatus(activity);
                case "WIFI_SSID":
                    return getWifiName(activity);
                case "WIFI_SPEED":
                    return getWifiLinkSpeed(activity);
                case "WIFI_STRENGTH":
                    return getwifiConnectionStrength(activity);
                case "WIFI_FREQUENCY":
                    return getwifiFrequency(activity);
                case "WIFI_DNS1":
                    return getWifiDNS1(activity);
                case "WIFI_DNS2":
                    return getWifiDNS2(activity);
                case "WIFI_GATEWAY":
                    return getWifiGateway(activity);
                case "WIFI_DHCP":
                    return getWifiDHCP(activity);
                case "WIFI_NETMASK":
                    return getwifiNetmask(activity);
                case "WIFI_LEASEDURATION":
                    return getwifiLeaseDuration(activity);





                case "DEVICE_TOTAL_MEMORY":
                    if (Build.VERSION.SDK_INT >= 16)
                        return String.valueOf(getTotalMemory(activity));
                case "DEVICE_FREE_MEMORY":
                    return String.valueOf(getFreeMemory(activity));
                case "DEVICE_USED_MEMORY":
                    if (Build.VERSION.SDK_INT >= 16) {
                        long freeMem = getTotalMemory(activity) - getFreeMemory(activity);
                        return String.valueOf(freeMem);
                    }
                    return "";
                case "DEVICE_TOTAL_CPU_USAGE":
                    int[] cpu = getCpuUsageStatistic();
                    if (cpu != null) {
                        int total = cpu[0] + cpu[1] + cpu[2] + cpu[3];
                        return String.valueOf(total);
                    }
                    return "";
                case "DEVICE_TOTAL_CPU_USAGE_SYSTEM":
                    int[] cpu_sys = getCpuUsageStatistic();
                    if (cpu_sys != null) {
                        int total = cpu_sys[1];
                        return String.valueOf(total);
                    }
                    return "";
                case "DEVICE_TOTAL_CPU_USAGE_USER":
                    int[] cpu_usage = getCpuUsageStatistic();
                    if (cpu_usage != null) {
                        int total = cpu_usage[0];
                        return String.valueOf(total);
                    }
                    return "";
                case "DEVICE_MANUFACTURE":
                    return android.os.Build.MANUFACTURER;
                case "DEVICE_SYSTEM_VERSION":
                    return String.valueOf(getDeviceName());
                case "DEVICE_VERSION":
                    return String.valueOf(android.os.Build.VERSION.SDK_INT);
                case "DEVICE_ID":
                    return getDeviceId(activity);
                case "DEVICE_IN_INCH":
                    return getDeviceInch(activity);
                case "DEVICE_TOTAL_CPU_IDLE":
                    int[] cpu_idle = getCpuUsageStatistic();
                    if (cpu_idle != null) {
                        int total = cpu_idle[2];
                        return String.valueOf(total);
                    }
                    return "";


                case "DEVICE_NETWORK_TYPE":
                    return getNetworkType(activity);
                case "DEVICE_NETWORK_ISROAMING":
                    return getNetworkIsRoaming(activity);
                case "DEVICE_NETWORK_CLASS":
                    return getNetworkClass(activity);
                case "DEVICE_NETWORK_METERED":
                    return getNetworkMetered(activity);
                case "DEVICE_NETWORK_OPERATORNAME":
                    return getNetworkOperator(activity);
                case "DEVICE_NETWORK_CID":
                    return getNetworkCid(activity);
                case "DEVICE_NETWORK_LAC":
                    return getNetworkLac(activity);
                case "DEVICE_NETWORK_MMC":
                    return getNetworkMcc(activity);
                case "DEVICE_NETWORK_MMSAGENT":
                    return getNetworkMMSAgent(activity);


                case "DEVICE_SIM_OPERATOR":
                    return getSIMOperator(activity);
                case "DEVICE_SIM_COUNTRYISO":
                    return getSIMCountryIso(activity);
                case "DEVICE_SIM_SERIELNUM":
                    return getSIMSerialNumber(activity);
                case "DEVICE_SIM_STATE":
                    return getSIMState(activity);
                case "DEVICE_SIM_IMSI":
                    return getSIMSubscriberID(activity);


                case "DEVICE_BATTERY_ISCHARGING":
                    return getBatteryIsCharging(activity);
                case "DEVICE_BATTERY_PROPERTY_CAPACITY":
                    return getBatteryPropertyCapacity(activity);
                case "DEVICE_BATTERY_PROPERTY_CHARGE_COUNTER":
                    return getBatteryProperyChargeCounter(activity);
                case "DEVICE_BATTERY_PROPERTY_CURRENT_NOW":
                    return getBatteryPropertyCurrentNOW(activity);
                case "DEVICE_BATTTERY_HEALTH":
                    return getBatteryHealth(activity);
                case "DEVICE_BATTERY_TECHNOLOGY":
                    return getBatteryTechnology(activity);
                case "DEVICE_BATTERY_TEMPERATURE":
                    return getBatteryTemperature(activity);
                case "DEVICE_BATTERY_VOLTAGE":
                    return getBatteryVoltage(activity);



                case "DEVICE_NETWORK_EXTRAINFO":
                    return getNetworkExtraInfo(activity);
                case "DEVICE_NETWORK":
                    return checkNetworkStatus(activity);
                case "IMEI":
                    return getIMEI(activity);
                case "DEVICE_TYPE":
                    if (isTablet(activity)) {
                        if (getDeviceMoreThan5Inch(activity)) {
                            return "Tablet";
                        } else
                            return "Mobile";
                    } else {
                        return "Mobile";
                    }
                default:
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getDeviceId(Context context) {
        String device_uuid = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        if (device_uuid == null) {
            device_uuid = "12356789"; // for emulator testing
        } else {
            try {
                byte[] _data = device_uuid.getBytes();
                MessageDigest _digest = java.security.MessageDigest.getInstance("MD5");
                _digest.update(_data);
                _data = _digest.digest();
                BigInteger _bi = new BigInteger(_data).abs();
                device_uuid = _bi.toString(36);
            } catch (Exception e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        }
        return device_uuid;
    }

    @SuppressLint("NewApi")
    private static long getTotalMemory(Context activity) {
        try {
            MemoryInfo mi = new MemoryInfo();
            ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(mi);
            long availableMegs = mi.totalMem / 1048576L; // in megabyte (mb)

            return availableMegs;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static long getFreeMemory(Context activity) {
        try {
            MemoryInfo mi = new MemoryInfo();
            ActivityManager activityManager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.getMemoryInfo(mi);
            long availableMegs = mi.availMem / 1048576L; // in megabyte (mb)

            return availableMegs;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /**
     * Convert byte array to hex string
     *
     * @param bytes
     * @return
     */
    private static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10)
                sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase());
        }
        return sbuf.toString();
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    @SuppressLint("NewApi")
    private static String getMACAddress(String interfaceName) {
        try {

            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName))
                        continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null)
                    return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0)
                    buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
            return "";
        } // for now eat exceptions
        return "";
            /*
             * try { // this is so Linux hack return
             * loadFileAsString("/sys/class/net/" +interfaceName +
             * "/address").toUpperCase().trim(); } catch (IOException ex) { return
             * null; }
             */
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @return address or empty string
     */
    private static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = true;//= InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port
                                // suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    /*
     *
     * @return integer Array with 4 elements: user, system, idle and other cpu
     * usage in percentage.
     */
    private static int[] getCpuUsageStatistic() {
        try {
            String tempString = executeTop();

            tempString = tempString.replaceAll(",", "");
            tempString = tempString.replaceAll("User", "");
            tempString = tempString.replaceAll("System", "");
            tempString = tempString.replaceAll("IOW", "");
            tempString = tempString.replaceAll("IRQ", "");
            tempString = tempString.replaceAll("%", "");
            for (int i = 0; i < 10; i++) {
                tempString = tempString.replaceAll("  ", " ");
            }
            tempString = tempString.trim();
            String[] myString = tempString.split(" ");
            int[] cpuUsageAsInt = new int[myString.length];
            for (int i = 0; i < myString.length; i++) {
                myString[i] = myString[i].trim();
                cpuUsageAsInt[i] = Integer.parseInt(myString[i]);
            }
            return cpuUsageAsInt;

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("executeTop", "error in getting cpu statics");
            return null;
        }
    }

    private static String executeTop() {
        java.lang.Process p = null;
        BufferedReader in = null;
        String returnString = null;
        try {
            p = Runtime.getRuntime().exec("top -n 1");
            in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while (returnString == null || returnString.contentEquals("")) {
                returnString = in.readLine();
            }
        } catch (IOException e) {
            Log.e("executeTop", "error in getting first line of top");
            e.printStackTrace();
        } finally {
            try {
                in.close();
                p.destroy();
            } catch (IOException e) {
                Log.e("executeTop", "error in closing and destroying top process");
                e.printStackTrace();
            }
        }
        return returnString;
    }

    public static String getNetworkType(final Context activity) {
        String networkStatus = "";

        ConnectivityManager network =(ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);;
        NetworkInfo netWorkInfo = network.getActiveNetworkInfo();
        return netWorkInfo.getTypeName();

    }

    public static String getNetworkExtraInfo(final Context activity) {
        String networkStatus = "";

        ConnectivityManager network =(ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);;
        NetworkInfo netWorkInfo = network.getActiveNetworkInfo();
        return netWorkInfo.getExtraInfo();

    }


    public static String getNetworkIsRoaming(final Context activity) {
        String networkStatus = "";

        ConnectivityManager network =(ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);;
        NetworkInfo netWorkInfo = network.getActiveNetworkInfo();
        return Boolean.toString(netWorkInfo.isRoaming());

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static String getNetworkMetered(final Context activity) {
        String networkStatus = "";

        ConnectivityManager network =(ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);;
        return Boolean.toString(network.isActiveNetworkMetered());

    }

    public static String getNetworkOperator(Context activity)
    {


        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();


        // Operator name
        String operatoprName = telephonyManager.getNetworkOperatorName();
        return operatoprName;
    }


    public static String getNetworkCid(Context activity)
    {


        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();

        // Cell Id, LAC
        int cellid = cellLocation.getCid();
       // int lac = cellLocation.getLac();


        return Integer.toString(cellid);
    }

    public static String getNetworkLac(Context activity)
    {


        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();

        // Cell Id, LAC
        //int cellid = cellLocation.getCid();
        int lac = cellLocation.getLac();

        return Integer.toString(lac);
    }

    public static String getNetworkMcc(Context activity)
    {


        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();

        // MCC
        String MCC = telephonyManager.getNetworkOperator();
        int mcc = Integer.parseInt(MCC.substring(0, 3));

        return Integer.toString(mcc);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getNetworkMMSAgent(Context activity)
    {


        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        GsmCellLocation cellLocation = (GsmCellLocation) telephonyManager.getCellLocation();


        return telephonyManager.getMmsUserAgent();
    }


    @TargetApi(Build.VERSION_CODES.M)
    public static String getBatteryIsCharging(Context activity)
    {
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        return Boolean.toString(batteryManager.isCharging());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getBatteryPropertyCapacity(Context activity)
    {
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        return Integer.toString(batteryManager.getIntProperty(batteryManager.BATTERY_PROPERTY_CAPACITY));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getBatteryProperyChargeCounter(Context activity)
    {
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        if(batteryManager.getIntProperty(batteryManager.BATTERY_PROPERTY_CHARGE_COUNTER) == 0){
            return "UNSUPPORTED";
        }

        else
            return Integer.toString(batteryManager.getIntProperty(batteryManager.BATTERY_PROPERTY_CHARGE_COUNTER));
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static String getBatteryPropertyCurrentNOW(Context activity)
    {
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        return Integer.toString(batteryManager.getIntProperty(batteryManager.BATTERY_PROPERTY_CURRENT_NOW)/1000);
    }


    public static String getBatteryHealth(Context activity)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = activity.registerReceiver(null, ifilter);;
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        switch(intent.getIntExtra(batteryManager.EXTRA_HEALTH, -1)){
            case 7:
                return "BATTERY_HEALTH_OLD";
            case 4:
                return "BATTERY_HEALTH_DEAD";
            case 2:
                return "BATTERY_HEALTH_GOOD";
            case 3:
                return "BATTERY_HEALTH_OVERHEAT";
            case 5:
                return "BATTERY_HEALTH_OVER_VOLTAGE";
            case 1:
                return "BATTERY_HEALTH_UNKOWN";
            default:
                return "UNKOWN ERROR";



        }

    }

    public static String getBatteryTechnology(Context activity)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = activity.registerReceiver(null, ifilter);;
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        return intent.getExtras().getString(batteryManager.EXTRA_TECHNOLOGY);
    }

    public static String getBatteryTemperature(Context activity)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = activity.registerReceiver(null, ifilter);;
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        return Integer.toString(intent.getIntExtra(batteryManager.EXTRA_TEMPERATURE, -1)/10) + "."+Integer.toString(intent.getIntExtra(batteryManager.EXTRA_TEMPERATURE, -1)%10);
    }

    public static String getBatteryVoltage(Context activity)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = activity.registerReceiver(null, ifilter);;
        BatteryManager batteryManager = (BatteryManager) activity.getSystemService(Context.BATTERY_SERVICE);
        return Integer.toString(intent.getIntExtra(batteryManager.EXTRA_VOLTAGE, -1)/1000)+"."+Integer.toString(intent.getIntExtra(batteryManager.EXTRA_VOLTAGE, -1)%1000);
    }



    public static String getNetworkClass(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = mTelephonyManager.getNetworkType();
        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "2G";
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "3G";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "4G";
            default:
                return "Unknown";
        }
    }









    public static String checkNetworkStatus(final Context activity) {
        String networkStatus = "";
        try {
            // Get connect mangaer
            final ConnectivityManager connMgr = (ConnectivityManager)
                    activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            // // check for wifi
            final android.net.NetworkInfo wifi =
                    connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            // // check for mobile data
            final android.net.NetworkInfo mobile =
                    connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (wifi.isAvailable()) {
                networkStatus = "Wifi";
            } else if (mobile.isAvailable()) {
                networkStatus = getDataType(activity);
            } else {
                networkStatus = "noNetwork";
                networkStatus = "0";
            }


        } catch (Exception e) {
            e.printStackTrace();
            networkStatus = "0";
        }
        return networkStatus;

    }






    public static String getSIMOperator(Context activity)
    {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkOperatorName();
    }

    public static String getSIMCountryIso(Context activity)
    {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkCountryIso();
    }

    public static String getSIMState(Context activity)
    {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        switch (telephonyManager.getSimState()) {
            case 0:
                return "SIM_STATE_UNKNOWN";
            case 1:
                return "SIM_STATE_ABSENT";
            case 2:
                return "SIM_STATE_PIN_REQUIRED";
            case 3:
                return "IM_STATE_PUK_REQUIRED";
            case 4:
                return "SIM_STATE_NETWORK_LOCKED";
            case 5:
                return "SIM_STATE_READY";
            default:
                return "unknown";



        }

    }

    public static String getSIMSubscriberID(Context activity)
    {

        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getSubscriberId();
    }


    public static String getSIMSerialNumber(Context activity)
    {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getSimSerialNumber();
    }































    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static boolean getDeviceMoreThan5Inch(Context activity) {
        try {
            DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();
            // int width = displayMetrics.widthPixels;
            // int height = displayMetrics.heightPixels;

            float yInches = displayMetrics.heightPixels / displayMetrics.ydpi;
            float xInches = displayMetrics.widthPixels / displayMetrics.xdpi;
            double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
            if (diagonalInches >= 7) {
                // 5inch device or bigger
                return true;
            } else {
                // smaller device
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static String getDeviceInch(Context activity) {
        try {
            DisplayMetrics displayMetrics = activity.getResources().getDisplayMetrics();

            float yInches = displayMetrics.heightPixels / displayMetrics.ydpi;
            float xInches = displayMetrics.widthPixels / displayMetrics.xdpi;
            double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);
            String str = String.valueOf(diagonalInches);
            str = String.format(str, "%.2f");
            return str;
        } catch (Exception e) {
            return "-1";
        }
    }




    public static String getDataType(Context activity) {
        String type = "Mobile Data";
        TelephonyManager tm = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        switch (tm.getNetworkType()) {
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                type = "Mobile Data 3G";
                Log.d("Type", "3g");
                // for 3g HSDPA networktype will be return as
                // per testing(real) in device with 3g enable
                // data
                // and speed will also matters to decide 3g network type
                break;
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                type = "Mobile Data 4G";
                Log.d("Type", "4g");
                // No specification for the 4g but from wiki
                // i found(HSPAP used in 4g)
                break;
            case TelephonyManager.NETWORK_TYPE_GPRS:
                type = "Mobile Data GPRS";
                Log.d("Type", "GPRS");
                break;
            case TelephonyManager.NETWORK_TYPE_EDGE:
                type = "Mobile Data EDGE 2G";
                Log.d("Type", "EDGE 2g");
                break;

        }

        return type;
    }

    public static String getIMEI(Context activity) {


        TelephonyManager mngr = (TelephonyManager) activity.getSystemService(activity.TELEPHONY_SERVICE);
        String imei = mngr.getDeviceId();
        //return imei;
        if (imei == null) {
            return "NOT AVAILABLE";
        } else {
            return imei;
        }


    }

    public static String getWifiIP(Context activity) {

        WifiManager wifiMgr = (WifiManager) activity.getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipAddress = Formatter.formatIpAddress(ip);
        return ipAddress;

    }

    public static String getWifiMAC(Context activity) {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }


    public static String getWifiName(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        return wifiInfo.getSSID();
    }


    public static String getWifiStatus(Context activity) {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        switch (wifiMgr.getWifiState()) {
            case WifiManager.WIFI_STATE_DISABLED:
                return "Wifi disabled";
            case WifiManager.WIFI_STATE_DISABLING:
                return "Wifi disabling";
            case WifiManager.WIFI_STATE_ENABLED:
                return "Wifi enabled";
            case WifiManager.WIFI_STATE_ENABLING:
                return "Wifi enabled";
            default:
                return "UNKNOWN";

        }
    }

    public static String getWifiLinkSpeed(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        return Integer.toString(wifiInfo.getLinkSpeed());

    }

    public static String getwifiConnectionStrength(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        return Integer.toString(wifiInfo.getRssi());

    }

    public static String getwifiFrequency(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        return Integer.toString(wifiInfo.getFrequency());
    }


    public static String getWifiDNS1(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiMgr.getDhcpInfo();
        return integerToIPAddress(dhcpInfo.dns1);

    }



    public static String getWifiDNS2(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiMgr.getDhcpInfo();
        return integerToIPAddress(dhcpInfo.dns2);

    }

    public static String getWifiGateway(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiMgr.getDhcpInfo();
        return integerToIPAddress(dhcpInfo.gateway);




    }


    public static String getWifiDHCP(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiMgr.getDhcpInfo();
        return integerToIPAddress(dhcpInfo.ipAddress);
    }


    public static String getwifiNetmask(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiMgr.getDhcpInfo();
        return integerToIPAddress(dhcpInfo.netmask);
    }

    public static String getwifiLeaseDuration(Context activity)
    {
        WifiManager wifiMgr = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiMgr.getDhcpInfo();
        return Integer.toString(dhcpInfo.leaseDuration);
    }

    public static String integerToIPAddress(int ip)
    {

        String ipStr =
                String.format("%d.%d.%d.%d",
                        (ip & 0xff),
                        (ip >> 8 & 0xff),
                        (ip >> 16 & 0xff),
                        (ip >> 24 & 0xff));
        return ipStr;
    }















}